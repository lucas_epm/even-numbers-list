package altran_test;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import filters.EvenNumberFilterLambdaLoop;
import filters.EvenNumberFilterNormalLoop;

public class Application {

	public static void main(String[] args) {
		List<Integer> numbersList = IntStream.rangeClosed(1, 10).boxed().collect(Collectors.toList()); 
		EvenNumberFilterLambdaLoop lambdaEvenNumberFilter = new EvenNumberFilterLambdaLoop();
		List<Integer> evenNumbersList = lambdaEvenNumberFilter.getEvenNumbersList(numbersList);

//		List<Integer> numbersList = Arrays.asList(1,2,3,4,5,6,7,8,9,10);
//		EvenNumberFilterNormalLoop normalEvenNumberFilter = new EvenNumberFilterNormalLoop();
//		List<Integer> evenNumbersList = normalEvenNumberFilter.getEvenNumbersList(numbersList);
		
		for (Integer evenNumber: evenNumbersList) {
			System.out.println(evenNumber);
		}

	}

}
