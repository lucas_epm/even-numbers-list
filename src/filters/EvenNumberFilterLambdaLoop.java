package filters;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class EvenNumberFilterLambdaLoop implements EvenNumberFilter {

	@Override
	public List<Integer> getEvenNumbersList(List<Integer> numbersList) {
		List<Integer> evenNumbersList = new ArrayList<Integer>();
		evenNumbersList = numbersList.stream().filter(evenNumber -> evenNumber % 2 == 0).collect(Collectors.toList());
		return evenNumbersList;
	}

}
