package filters;

import java.util.List;

public interface EvenNumberFilter {
	
	public List<Integer> getEvenNumbersList(List<Integer> numbersList);
}
