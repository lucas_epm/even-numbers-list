package filters;

import java.util.ArrayList;
import java.util.List;

public class EvenNumberFilterNormalLoop implements EvenNumberFilter {

	@Override
	public List<Integer> getEvenNumbersList(List<Integer> numbersList) {
		int numbersListSize = numbersList.size();
		List<Integer> evenNumbersList = new ArrayList<Integer>();
		
		for (int numbersListIndex = 0; numbersListIndex < numbersListSize; numbersListIndex++) {
			Integer numberIndexValue = numbersList.get(numbersListIndex);
			if (numberIndexValue % 2 == 0) {
				evenNumbersList.add(numberIndexValue);
			}
		}
		return evenNumbersList;
	}

}
